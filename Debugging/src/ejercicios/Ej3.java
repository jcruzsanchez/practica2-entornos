package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		Scanner lector;
		char caracter=27;
		int posicionCaracter;
		String cadenaLeida, cadenaFinal;
		lector = new Scanner(System.in);
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();
		
		posicionCaracter = cadenaLeida.indexOf(caracter);
		
		if(posicionCaracter > -1){
		cadenaFinal = cadenaLeida.substring(0, posicionCaracter);
		/*
		 * El problema se debe a que si la cadena introducida no contiene el char con c�digo ascii n� 27, el m�todo substring dar� error ya que el 2� par�metro que se
		 * le pasa el -1
		 */
		/*
		 * Una soluci�n ser�a poner un condicional antes de la instrucci�n n� 18 que eval�e si posici�nCaracter es >= que 0
		 */
		
		System.out.println(cadenaFinal);
		}
		
		lector.close();
	}

}
