package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		input.nextLine();
		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();
		
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			/*
			 * El problema se debe a que que variable cadenaLeida coge restos de informacion ya que el buffer no fue limpiado. 
			 * En este caso \n. Y el error, en la linea 23 es debido a que no se puede hacer un parse a entero de "\n"
			 */
			/*
			 * Soluci�n puede ser a�adir una linea despues de la linea 18 que sea como sigue: "input.nextLine();".
			 */
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		}
		
		input.close();
		
		
		
	}
}
