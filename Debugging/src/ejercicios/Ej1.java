package ejercicios;

import java.util.Scanner;

public class Ej1 {

	public static void main(String[] args) {
		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;
		
		cantidadVocales = 0;
		cantidadCifras = 0;
		
		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();
		
		for(int i = 0 ; i < cadenaLeida.length(); i++){
			caracter = cadenaLeida.charAt(i);
			/*
			 * La excepci�n es debida a que el valor de la variable i es mayor que el tama�o de la cadena lo que produce la excepci�n 
			 * fuera de rango. 
			 */
			/*
			 * Se arreglar�a ajustando el encabezado del bucle: for(int i = 0 ; i < cadenaLeida.length(); i++)
			 */
			if(caracter == 'a' || caracter == 'e' || caracter == 'i' 
					|| caracter == 'o' || caracter == 'u'){
					cantidadVocales++;
			}
			if(caracter >= '0' && caracter <='9'){
				cantidadCifras++;
			}
			
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);
		
		
		input.close();
	}

}
