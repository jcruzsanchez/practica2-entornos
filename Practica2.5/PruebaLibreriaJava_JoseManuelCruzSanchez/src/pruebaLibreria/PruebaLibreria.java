package pruebaLibreria;

import libreria.Libreria_1;
import libreria.Libreria_2;

public class PruebaLibreria {

	public static void main(String[] args) {
		System.out.println("Trabajo con la libreria de cadenas.");
		Libreria_1.concatena("Hello ", "World");
		Libreria_1.contarLetras("Amigo de vigo");
		Libreria_1.listar("Espejo");
		Libreria_1.quitarTexto("suegro", "g");
		Libreria_1.segundaLetra("o@#o");
		System.out.println("Trabajo con la libreria num�rica.");
		Libreria_2.divide(8, 4);
		Libreria_2.muestraMenores(12);
		Libreria_2.multiplica(3, 3);
		Libreria_2.resta(12, 75);
		Libreria_2.suma(25, 35);

	}

}
