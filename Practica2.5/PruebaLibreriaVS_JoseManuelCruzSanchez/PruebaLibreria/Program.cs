﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibreriaVS_JoseManuelCruzSanchez;

namespace PruebaLibreria
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Uso de la clase de métodos numéricos.");
            Libreria1_Integer.suma(3, 5);
            Libreria1_Integer.resta(3, 5);
            Libreria1_Integer.multiplica(3, 5);
            Libreria1_Integer.multiplica(15, 3);
            Libreria1_Integer.muestraMenores(12);
            Console.WriteLine("Uso de la clase de métodos cadenas.");
            Libreria2_String.concatena("Hello ", "World!");
            Libreria2_String.contarLetras("Cantidad de letras");
            Libreria2_String.quitarTexto("Hola compañeros de clase", "clase");
            Libreria2_String.segundaLetra("p@#");
            Libreria2_String.listar("Esternocleidomastoideo");
            Console.ReadKey();
            /*String cadena = LibreriaVS_JoseManuelCruzSanchez.Libreria2_String.invertirCadena("hola");
            Console.WriteLine(cadena);
            bool primo = LibreriaVS_JoseManuelCruzSanchez.Libreria1_Integer.esPrimo(7);
            Console.WriteLine(primo);
            Console.WriteLine("pulsa tecla para continuar");
            Console.ReadKey();*/

            /*LibreriaVS_JoseManuelCruzSanchez.Libreria1_Integer.esPrimo(7);*/
         
        }
    }
}
