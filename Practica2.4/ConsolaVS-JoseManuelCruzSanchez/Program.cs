﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConsolaVS_JoseManuelCruzSanchez
{
    class Program
    {
        static void Main(string[] args)
        {
            String opcion;
            System.Console.WriteLine("Selecciona una opcion:");
            System.Console.WriteLine("1 - Suma números");
            System.Console.WriteLine("2 - Resta números");
            System.Console.WriteLine("3 - Multiplica números");
            System.Console.WriteLine("4 - Divide números");
            opcion = System.Console.ReadLine();
            switch (opcion)
            {
                case "1":
                    suma();
                    break;
                case "2":
                    resta();
                    break;
                case "3":
                    multiplica();
                    break;
                case "4":
                    divide();
                    break;
            }
        }
        static void suma()
        {
            int x, y;
            System.Console.WriteLine("Introduce el primer número:");
            x = int.Parse(System.Console.ReadLine());
            System.Console.WriteLine("Introduce el segundo número:");
            y = int.Parse(System.Console.ReadLine());
            System.Console.WriteLine("Resultado: " + (x + y));
            System.Console.WriteLine("Pulse una tecla para continuar...");
            Console.ReadKey();
        }
        static void resta()
        {
            int x, y;
            System.Console.WriteLine("Introduce el primer número:");
            x = int.Parse(System.Console.ReadLine());
            System.Console.WriteLine("Introduce el segundo número:");
            y = int.Parse(System.Console.ReadLine());
            System.Console.WriteLine("Resultado: " + (x - y));
            System.Console.WriteLine("Pulse una tecla para continuar...");
            Console.ReadKey();
        }
        static void multiplica()
        {
            int x, y;
            System.Console.WriteLine("Introduce el primer número:");
            x = int.Parse(System.Console.ReadLine());
            System.Console.WriteLine("Introduce el segundo número:");
            y = int.Parse(System.Console.ReadLine());
            System.Console.WriteLine("Resultado: " + (x * y));
            System.Console.WriteLine("Pulse una tecla para continuar...");
            Console.ReadKey();
        }
        static void divide()
        {
            int x, y;
            System.Console.WriteLine("Introduce el primer número:");
            x = int.Parse(System.Console.ReadLine());
            System.Console.WriteLine("Introduce el segundo número:");
            y = int.Parse(System.Console.ReadLine());
            System.Console.WriteLine("Resultado: " + (x / y));
            System.Console.WriteLine("Pulse una tecla para continuar...");
            Console.ReadKey();
        }
    }

}
