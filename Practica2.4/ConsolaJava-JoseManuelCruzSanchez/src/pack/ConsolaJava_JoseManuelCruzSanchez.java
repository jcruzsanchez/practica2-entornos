package pack;

import java.util.Scanner;

public class ConsolaJava_JoseManuelCruzSanchez {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		int opcion;
		System.out.println("Selecciona una opcion:");
		System.out.println("1 - Suma n�meros");
		System.out.println("2 - Resta n�meros");
		System.out.println("3 - Multiplica n�meros");
		System.out.println("4 - Divide n�meros");
		opcion = lector.nextInt();
		switch (opcion) {
		case 1:
			suma(lector);
			break;
		case 2:
			resta(lector);
			break;
		case 3:
			multiplica(lector);
			break;
		case 4:
			divide(lector);
			break;
		default:
			break;
		}
		
		
		lector.close();
	}
	static void suma(Scanner lector){
		int x, y;
		System.out.println("Introduce el primer n�mero:");
		x = lector.nextInt();
		System.out.println("Introduce el segundo n�mero:");
		y = lector.nextInt();
		System.out.println("Resultado: " + (x+y));
	}
	static void resta(Scanner lector){
		int x, y;
		System.out.println("Introduce el primer n�mero:");
		x = lector.nextInt();
		System.out.println("Introduce el segundo n�mero:");
		y = lector.nextInt();
		System.out.println("Resultado: " + (x-y));
	}
	static void multiplica(Scanner lector){
		int x, y;
		System.out.println("Introduce el primer n�mero:");
		x = lector.nextInt();
		System.out.println("Introduce el segundo n�mero:");
		y = lector.nextInt();
		System.out.println("Resultado: " + (x*y));
	}
	static void divide(Scanner lector){
		int x, y;
		System.out.println("Introduce el primer n�mero:");
		x = lector.nextInt();
		System.out.println("Introduce el segundo n�mero:");
		y = lector.nextInt();
		System.out.println("Resultado: " + (x/y));
	}

}
